import os
import unittest

import numpy
import yaml

import d3m
from d3m import utils
from d3m.metadata.pipeline_run.runtime_environment import (
    RuntimeEnvironment, D3M_BASE_IMAGE_NAME, D3M_BASE_IMAGE_DIGEST, D3M_IMAGE_NAME, D3M_IMAGE_DIGEST
)
from d3m.metadata.pipeline_run.score import Score


class TestComputeResources(unittest.TestCase):
    # todo
    pass


class TestRuntimeEnvironment(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.repo_path = os.path.realpath(d3m.__file__).rsplit('d3m',1)[0]
        cls.original_git_path = os.path.join(cls.repo_path, '.git')
        cls.moved_git_path = os.path.join(cls.repo_path, '.git_moved')

    @classmethod
    def tearDown(cls):
        if os.path.exists(cls.moved_git_path):
            os.rename(cls.moved_git_path, cls.original_git_path)

    def test_empty_instantiation(self):
        with utils.silence():
            RuntimeEnvironment()

    def test_deterministic_id(self):
        with utils.silence():
            env = RuntimeEnvironment()
        json_structure = env.to_json_structure()
        id_ = json_structure.pop('id')
        gen_id = utils.compute_hash_id(json_structure)
        self.assertEqual(id_, gen_id, 'environment.id not deterministically generated')

    def _set_env_vars(self):
        os.environ[D3M_BASE_IMAGE_NAME] = 'D3M_BASE_IMAGE_NAME_VALUE'
        os.environ[D3M_BASE_IMAGE_DIGEST] = 'D3M_BASE_IMAGE_DIGEST_VALUE'
        os.environ[D3M_IMAGE_NAME] = 'D3M_IMAGE_NAME_VALUE'
        os.environ[D3M_IMAGE_DIGEST] = 'D3M_IMAGE_DIGEST_VALUE'

    def _del_env_vars(self):
        del os.environ[D3M_BASE_IMAGE_NAME]
        del os.environ[D3M_BASE_IMAGE_DIGEST]
        del os.environ[D3M_IMAGE_NAME]
        del os.environ[D3M_IMAGE_DIGEST]

    def _test_env_vars(self):
        self._set_env_vars()
        try:
            env = RuntimeEnvironment()
            json_structure = env.to_json_structure()

            self.assertEqual(
                json_structure['base_docker_image']['image_name'],
                os.environ[D3M_BASE_IMAGE_NAME],
                'base_image_name incorrectly extracted from environment variables'
            )
            self.assertEqual(
                json_structure['base_docker_image']['image_digest'],
                os.environ[D3M_BASE_IMAGE_DIGEST],
                'base_image_digest incorrectly extracted from environment variables'
            )
            self.assertEqual(
                json_structure['docker_image']['image_name'],
                os.environ[D3M_IMAGE_NAME],
                'image_name incorrectly extracted from environment variables'
            )
            self.assertEqual(
                json_structure['docker_image']['image_digest'],
                os.environ[D3M_IMAGE_DIGEST],
                'image_digest incorrectly extracted from environment variables'
            )

        finally:
            self._del_env_vars()

    def test_no_git_repo(self):
        git_path_moved = False
        if os.path.exists(self.original_git_path):
            os.rename(self.original_git_path, self.moved_git_path)
            git_path_moved = True
        try:
            with utils.silence():
                env = RuntimeEnvironment()
            json_structure = env.to_json_structure()

            self.assertEqual(
                json_structure['reference_engine_version'], d3m.__version__,
                'reference_engine_version incorrectly extracted from d3m repo'
            )
        finally:
            if git_path_moved:
                os.rename(self.moved_git_path, self.original_git_path)


class TestPipelineRunScore(unittest.TestCase):
    def test_score_basic(self):
        metric = {'metric': 'MEAN_SQUARED_ERROR'}
        value = numpy.float32(2.220446049250313e-16)
        dataset_id = 'test_dataset_id'
        targets = [{
            'column_index': 8,
            'column_name': 'class',
            'resource_id': 'learningData',
            'target_index': 0,
        }]

        score = Score(metric=metric, value=value, dataset_id=dataset_id, targets=targets)

        expected_result = {
            'dataset_id': 'test_dataset_id',
            'metric': {'metric': 'MEAN_SQUARED_ERROR'},
            'targets': [
                {
                    'column_index': 8,
                    'column_name': 'class',
                    'resource_id': 'learningData',
                    'target_index': 0,
                },
            ],
            'value': 2.220446049250313e-16,
        }
        self.assertDictEqual(yaml.safe_load(yaml.safe_dump(score.to_json_structure())), expected_result)


if __name__ == '__main__':
    unittest.main()
