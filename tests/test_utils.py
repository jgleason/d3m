import builtins
import logging
import sys
import typing
import unittest

import numpy
import yaml

from d3m import container, types, utils
from d3m.container import list
from d3m.metadata import base as metadata_base


class TestUtils(unittest.TestCase):
    def test_get_type_arguments(self):
        A = typing.TypeVar('A')
        B = typing.TypeVar('B')
        C = typing.TypeVar('C')

        class Base(typing.Generic[A, B]):
            pass

        class Foo(Base[A, None]):
            pass

        class Bar(Foo[A], typing.Generic[A, C]):
            pass

        class Baz(Bar[float, int]):
            pass

        self.assertEqual(utils.get_type_arguments(Bar), {
            A: typing.Any,
            B: type(None),
            C: typing.Any,
        })
        self.assertEqual(utils.get_type_arguments(Baz), {
            A: float,
            B: type(None),
            C: int,
        })

        self.assertEqual(utils.get_type_arguments(Base), {
            A: typing.Any,
            B: typing.Any,
        })

        self.assertEqual(utils.get_type_arguments(Base[float, int]), {
            A: float,
            B: int,
        })

        self.assertEqual(utils.get_type_arguments(Foo), {
            A: typing.Any,
            B: type(None),
        })

        self.assertEqual(utils.get_type_arguments(Foo[float]), {
            A: float,
            B: type(None),
        })

    def test_issubclass(self):
        self.assertTrue(utils.is_subclass(list.List, types.Container))

        T1 = typing.TypeVar('T1', bound=list.List)
        self.assertTrue(utils.is_subclass(list.List, T1))

    def test_create_enum(self):
        obj = {
            'definitions': {
                'foobar1':{
                    'type': 'array',
                    'items': {
                        'anyOf':[
                            {'enum': ['AAA']},
                            {'enum': ['BBB']},
                            {'enum': ['CCC']},
                            {'enum': ['DDD']},
                        ],
                    },
                },
                'foobar2': {
                    'type': 'array',
                    'items': {
                        'type': 'object',
                        'anyOf': [
                            {
                                'properties': {
                                    'type': {
                                        'type': 'string',
                                        'enum': ['EEE'],
                                    },
                                },
                            },
                            {
                                'properties': {
                                    'type': {
                                        'type': 'string',
                                        'enum': ['FFF'],
                                    },
                                },
                            },
                            {
                                'properties': {
                                    'type': {
                                        'type': 'string',
                                        'enum': ['GGG'],
                                    },
                                },
                            },
                        ],
                    },
                },
                'foobar3': {
                    'type': 'string',
                    'enum': ['HHH', 'HHH', 'III', 'JJJ'],
                }
            },
        }

        Foobar1 = utils.create_enum_from_json_schema_enum('Foobar1', obj, 'definitions.foobar1.items.anyOf[*].enum[*]')
        Foobar2 = utils.create_enum_from_json_schema_enum('Foobar2', obj, 'definitions.foobar2.items.anyOf[*].properties.type.enum[*]')
        Foobar3 = utils.create_enum_from_json_schema_enum('Foobar3', obj, 'definitions.foobar3.enum[*]')

        self.assertSequenceEqual(builtins.list(Foobar1.__members__.keys()), ['AAA', 'BBB', 'CCC', 'DDD'])
        self.assertSequenceEqual([value.value for value in Foobar1.__members__.values()], [1, 2, 3, 4])

        self.assertSequenceEqual(builtins.list(Foobar2.__members__.keys()), ['EEE', 'FFF', 'GGG'])
        self.assertSequenceEqual([value.value for value in Foobar2.__members__.values()], [1, 2, 3])

        self.assertSequenceEqual(builtins.list(Foobar3.__members__.keys()), ['HHH', 'III', 'JJJ'])
        self.assertSequenceEqual([value.value for value in Foobar3.__members__.values()], [1, 2, 3])

        self.assertTrue(Foobar1.AAA.name == 'AAA')
        self.assertTrue(Foobar1.AAA.value == 1)
        self.assertTrue(Foobar1.AAA == Foobar1.AAA)
        self.assertTrue(Foobar1.AAA == 'AAA')

    def test_redirect(self):
        logger = logging.getLogger('test_logger')
        with self.assertLogs(logger=logger, level=logging.DEBUG) as cm:
            with utils.redirect_to_logging(logger=logger):
                print("Test.")

        self.assertEqual(len(cm.records), 1)
        self.assertEqual(cm.records[0].message, "Test.")

        logger2 = logging.getLogger('test_logger2')
        with self.assertLogs(logger=logger, level=logging.DEBUG) as cm:
            with self.assertLogs(logger=logger2, level=logging.DEBUG) as cm2:
                with utils.redirect_to_logging(logger=logger):
                    print("Test.")
                    with utils.redirect_to_logging(logger=logger2):
                        print("Test2.")

        self.assertEqual(len(cm.records), 1)
        self.assertEqual(cm.records[0].message, "Test.")
        self.assertEqual(len(cm2.records), 1)
        self.assertEqual(cm2.records[0].message, "Test2.")

        records = []

        def callback(record):
            nonlocal records
            records.append(record)

        # Test recursion prevention.
        with self.assertLogs(logger=logger, level=logging.DEBUG) as cm:
            with self.assertLogs(logger=logger2, level=logging.DEBUG) as cm2:
                # We add it twice so that we test that handler does not modify record while running.
                logger2.addHandler(utils.CallbackHandler(callback))
                logger2.addHandler(utils.CallbackHandler(callback))

                with utils.redirect_to_logging(logger=logger):
                    print("Test.")
                    with utils.redirect_to_logging(logger=logger2):
                        # We configure handler after redirecting.
                        handler = logging.StreamHandler(sys.stdout)
                        handler.setFormatter(logging.Formatter('Test format: %(message)s'))
                        logger2.addHandler(handler)
                        print("Test2.")

        # We use outer "redirect_to_logging" to make sure nothing from inner gets out.
        self.assertEqual(len(cm.records), 1)
        self.assertEqual(cm.records[0].message, "Test.")

        self.assertEqual(len(cm2.records), 2)
        # This one comes from the print.
        self.assertEqual(cm2.records[0].message, "Test2.")
        # And this one comes from the stream handler.
        self.assertEqual(cm2.records[1].message, "Test format: Test2.")

        self.assertEqual(len(records), 4)
        self.assertEqual(records[0]['message'], "Test2.")
        self.assertEqual(records[1]['message'], "Test2.")
        self.assertEqual(records[2]['message'], "Test format: Test2.")
        self.assertEqual(records[3]['message'], "Test format: Test2.")

    def test_columns_sum(self):
        dataframe = container.DataFrame({'a': [1, 2, 3], 'b': [4, 5, 6]})

        dataframe_sum = utils.columns_sum(dataframe)

        self.assertIs(dataframe_sum.metadata.for_value, dataframe_sum)
        self.assertEqual(dataframe_sum.values.tolist(), [[6, 15]])
        self.assertEqual(dataframe_sum.metadata.query((metadata_base.ALL_ELEMENTS, 0))['name'], 'a')
        self.assertEqual(dataframe_sum.metadata.query((metadata_base.ALL_ELEMENTS, 1))['name'], 'b')

        array = container.ndarray(dataframe)

        array_sum = utils.columns_sum(array)

        self.assertIs(array_sum.metadata.for_value, array_sum)
        self.assertEqual(array_sum.tolist(), [[6, 15]])
        self.assertEqual(array_sum.metadata.query((metadata_base.ALL_ELEMENTS, 0))['name'], 'a')
        self.assertEqual(array_sum.metadata.query((metadata_base.ALL_ELEMENTS, 1))['name'], 'b')

    def test_numeric(self):
        self.assertTrue(utils.is_float(type(1.0)))
        self.assertFalse(utils.is_float(type(1)))
        self.assertFalse(utils.is_int(type(1.0)))
        self.assertTrue(utils.is_int(type(1)))
        self.assertTrue(utils.is_numeric(type(1.0)))
        self.assertTrue(utils.is_numeric(type(1)))

    def test_yaml_representers(self):
        self.assertEqual(yaml.load(yaml.safe_dump(numpy.int32(1))), 1)
        self.assertEqual(yaml.load(yaml.safe_dump(numpy.int64(1))), 1)
        self.assertEqual(yaml.load(yaml.safe_dump(numpy.float32(1.0))), 1.0)
        self.assertEqual(yaml.load(yaml.safe_dump(numpy.float64(1.0))), 1.0)


if __name__ == '__main__':
    unittest.main()
